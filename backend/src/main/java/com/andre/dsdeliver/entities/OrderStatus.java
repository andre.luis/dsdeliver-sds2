package com.andre.dsdeliver.entities;

public enum OrderStatus {

    PENDING, DELIVERED;
}
