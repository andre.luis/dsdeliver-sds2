import './styles.css'
import { ReactComponent as YoutubeIcon } from './youtube.svg'
import { ReactComponent as InstagranIcon } from './instagran.svg'
import { ReactComponent as LinkedinIcon } from './linkedin.svg'

function Footer() {
    return(
       <footer className="main-footer">
           App desenvolvido durante a 2ª ed. do evento Semana DevSuperior
       <div className="footer-icons">
           <a href="https://www.youtube.com/c/DevSuperior" target="_new">
                <YoutubeIcon />
           </a>
           <a href="https://www.linkedin.com/in/andr%C3%A9-luis-5267b5129/" target="_new">
                <LinkedinIcon />
           </a>
           <a href="https://www.instagram.com/devsuperior.ig">
                <InstagranIcon />
           </a>
       </div>
       </footer>
    )
}

export default Footer;